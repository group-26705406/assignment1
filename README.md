#Overview
The project is an online platform for businnesses to handle day-to-day proceeedings inclusive of orders, deliveries, advertisments, customer service, marketing,etc.

#Functionality

    ##User Accounts

        #BusinessAccount
        Businesses create accounts with the following criteria specified:
            1.Name
            2.Contact Details
            3.Location(s)
            4.Business description
        Businesses will be able to upload logos,photos and videos to describe products and services.
        The platform will provide avenues for submission of menus, service listing with appointment slots and prices indicating availability of products

        #Customer Accounts
        Browsing businesses by category, product and location
        Viewing business profiles, menus, service descriptions, appointment schedules
        Placing orders online for delivery or pickups
        Booking appointments for services
        View and leave ratings, Organise businesses and products im accordance to ratings

        #Admin account
        Managing above user accounts
        Monitor platform activity(orders,bookings, reviews)
        Manage platform settings and  configurations
    
    ##Features
    Shopping Cart to compile orders,edit orders,update quantities and proceed to checkout
    Secure checkout process
    Contact and support
    Wishlist for customer accounts to store products
    



#Technicality

    ##Backend
    Django Framework for server side development
    Programming languages: Python

    ##Frontend
    Programming languages: Python, JavaScript

    ##Database
    MySQL
    PostgreSQL for storing user, business and service data

    ##Email API
    Mailgun
    sendGrid

    ##APIs
    Payment gateway APIs : MTN Momo Pay & Airtel Pay

    #IDE
    Visual studio code
    Gitlab for repo storage
    
    #AWS for cloud hosting services