from django.shortcuts import render, redirect
from .models import Product
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from django import forms

def index(request):
    products= Product.objects.all()
    return render(request, 'core/index.html',{'products':products})
# Create your views here.

def product(request,pk):
    product= Product.objects.get(id=pk)
    return render(request, 'core/product.html',{'products':product})

def base(request):
    return render(request, 'core/base.html') 

def register_user(request):
    return render(request, 'core/register.html') 

def login_user(request):
    if request.method=="POST":
        username=request.POST['username']
        password=request.POST['password']
        user    =authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            messages.success(request,("You have been logged in!"))
            return redirect('core:index')
        else:
            messages.success(request,("There was an error!"))
    else:
        return render(request, 'core/login.html',{}) 


def logout_user(request):
    logout(request)
    messages.success(request,("You have been logged out!"))
    return render(request, 'core/logout.html')

