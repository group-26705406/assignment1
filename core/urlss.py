from django.urls import path

from . import views

app_name="core"

urlpatterns = [
    path("",views.index, name='index'),
    #empty string implies no url pattern thus first page
    path("base/",views.base),
    path("login/",views.login_user, name="login"),
    path("logout/",views.logout_user, name="logout"),
    path("register/",views.register_user, name="register"),
    # path('product/<int:pk>', views.product, name='product'),
    # path('homepage', views.homepage, name='homepage'),

]
  