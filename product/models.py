from django.db import models

class Product(models.Model):
    Name       =models.CharField(max_length=20)
    Description=models.TextField(blank=True, null= True)
    Price      =models.DecimalField(max_digits=5, decimal_places=2)

# Create your models here.
